// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getAnalytics} from "firebase/analytics";
import {getFirestore} from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAw6mNwltJP7uiOuvscF3izS0sf3LGSBPs",
  authDomain: "crudlibro.firebaseapp.com",
  projectId: "crudlibro",
  storageBucket: "crudlibro.appspot.com",
  messagingSenderId: "80176634812",
  appId: "1:80176634812:web:6e57a2c0c8ebfdc9d6a9b9"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app)
const db = getFirestore(app)
export {db}